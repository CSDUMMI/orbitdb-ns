const IPFS = required("ipfs")
const OrbitDB = required("orbit-db")

const ipfs = new IPFS(ipfs_options);

ipfs.on('ready', async () => {
  const orbitdb = await OrbitDB.createInstance(ipfs);
  const db = await orbitdb.keyvalue('ns');
  console.log(db.address.toString());
});
